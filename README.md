# Hotel

![Hotel Board Game](image-1.png)

This project aims to implement an online version of the Hotel board game.
It's a family tradition to play it for christmas, but we couldn't play the real version because of Covid lockdown.

Most of the backend work is done, and the frontend is yet to be done (only debug for now).
I aim to use ThreeJS for the board in the future.

## Multi-modality

- TV / Computers / Tablets can be used to display the board for every players, sharing public informations
- Phones will be used to show private informations (money, actions, contextual informations) for players

## Technical Highlights

- Socket.io is used to create dedicated rooms/lobbies.
- Game state is stored on the server.
- Redux is used server side (while being typically a client library).
  - It makes debugging and setting an exact state very easy
  - Code is easy to read. All the rules are here: `server/src/mainSlice.ts`, and only answers the questions: "In this state, receiving this action, what happens?".
  - Players are only sending actions and minimal payloads, and will receive an updated state
    - This prevents cheating. Players can only inform the server that they are rolling the dice, but the server is actually randomly picking a number.
    - If the action is illegal (not the right moment, etc...) the game state won't be corrupted
- Shake your phone to roll the dice (thanks to the custom `client/src/hooks/useShake.ts`)

![shapes](shapes.png)

## Demo

![HotelDemo](./HotelDemo.mp4)
