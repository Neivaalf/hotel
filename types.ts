export type Path = {
  initialMoney: number
  squares: Square[]
  hotels: {
    [key in Hotel["name"]]: Hotel
  }
  collectMoneyIndex: number
  buyEntrancesIndex: number
}

export type Board = {
  gamePhase: GamePhase
  players: Player[]
  turnPlayer: Player | null
  buildingPermission?: {
    hotel: Hotel["name"]
    progress: Exclude<keyof Hotel["buildingPrices"], "FACILITIES">
  } | null
  path: Path
}

export type Player = {
  name: string
  color: Color
  roll: number | null
  permissionRoll: "FREE" | "DOUBLE" | "RED" | "GREEN" | null
  canRoll: boolean
  canClaimMoney: boolean
  canBuy: Array<Hotel["name"]>
  position: number
  money: number
}

export type Color = "BLUE" | "GREEN" | "YELLOW" | "RED"

export type SquareType =
  | "START"
  | "BUILD"
  | "BUY"
  | "FREE_BUILD"
  | "FREE_ENTRANCE"

export type Hotel = {
  name:
    | "BOOMERANG"
    | "FUJIYAMA"
    | "ETOILE"
    | "PRESIDENT"
    | "ROYAL"
    | "WAIKIKI"
    | "TAJ_MAHAL"
    | "SAFARI"
  costOfLand: number
  entrancePrice: number
  buildingPrices: {
    MAIN_BUILDING: number
    EXTENSION1?: number
    EXTENSION2?: number
    EXTENSION3?: number
    EXTENSION4?: number
    FACILITIES: number
  }
  rentDuePerNight: {
    MAIN_BUILDING: number
    EXTENSION1?: number
    EXTENSION2?: number
    EXTENSION3?: number
    EXTENSION4?: number
    FACILITIES: number
  }
  owner?: Player["name"]
  progress:
    | "EMPTY"
    | "MAIN_BUILDING"
    | "EXTENSION1"
    | "EXTENSION2"
    | "EXTENSION3"
    | "EXTENSION4"
    | "FACILITIES"
}

export type Square = {
  type: SquareType
  hotels: Array<Hotel["name"] | null>
}

export type GamePhase =
  | "JOINING"
  | "PLAYER_ORDERING"
  | "TURN_ROLL_MOVEMENT"
  | "TURN_ROLL_PERMISSION_BUILD"
  | "TURN_ACTION_BUY"
  | "TURN_ACTION_BUILD"
