import { PayloadAction, SliceCaseReducers } from "@reduxjs/toolkit"
import type { Board, Hotel, Player } from "../../types"

import { createSlice } from "@reduxjs/toolkit"

import { initialState } from "../../client/src/isomorphic"

const mainSlice = createSlice<Board, SliceCaseReducers<Board>>({
  name: "main",
  initialState,
  reducers: {
    addPlayer: (
      state,
      action: PayloadAction<Pick<Player, "name" | "color">>
    ) => {
      state.players.push({
        ...action.payload,
        roll: null,
        permissionRoll: null,
        canRoll: true,
        canClaimMoney: false,
        position: -1,
        money: state.path.initialMoney,
        canBuy: []
      })
      return state
    },
    startGame: (state) => {
      state.gamePhase = "PLAYER_ORDERING"
      return state
    },
    roll: (state, action) => {
      const currentPlayer = state.players.find(
        (player) => player.name === action.payload.playerName
      )

      switch (state.gamePhase) {
        case "PLAYER_ORDERING":
          currentPlayer.roll = action.payload.roll
          currentPlayer.canRoll = false

          const doesEverybodyRolled =
            state.players.filter((player) => player.canRoll === true).length ===
            0

          // Everybody rolled
          if (doesEverybodyRolled) {
            const rolls = state.players.map((player) => player.roll)

            const bestRoll = Math.max(...rolls)

            // We have a winner
            if (rolls.filter((roll) => roll === bestRoll).length === 1) {
              const bestPlayer = state.players.find(
                (player) => player.roll === bestRoll
              )

              bestPlayer.canRoll = true
              state.turnPlayer = bestPlayer
              state.gamePhase = "TURN_ROLL_MOVEMENT"

              state.players.forEach((player) => {
                player.canRoll = false
              })
            }
            // We need a winner
            else {
              const bestPlayers = state.players.filter(
                (player) => player.roll === bestRoll
              )

              const worstPlayers = state.players.filter(
                (player) => player.roll !== bestRoll
              )

              // Best players can reroll
              bestPlayers.forEach((player) => {
                player.canRoll = true
              })

              // Worst players rolled 0 (meaning they are out the competition)
              worstPlayers.forEach((player) => {
                player.roll = 0
              })
            }
          }

          break
        case "TURN_ROLL_MOVEMENT":
          const currentPlayerIndex = state.players.findIndex(
            (player) => player === currentPlayer
          )

          // Previous player can't claim money anymore
          const fuckJSModulo = (a, m) => {
            return ((a % m) + m) % m
          }

          const lastIndex = fuckJSModulo(
            currentPlayerIndex - 1,
            state.players.length
          )

          state.players[lastIndex].canClaimMoney = false

          // Display current roll
          currentPlayer.roll = action.payload.roll

          // Minimum new position
          let newPosition = currentPlayer.position + action.payload.roll

          // If the square is already busy, keep going until the next free one
          const positions = state.players.map((player) => player.position)
          while (positions.includes(newPosition)) {
            newPosition++
          }

          // Current player can claim money if he just crossed the line
          const { collectMoneyIndex } = state.path
          currentPlayer.canClaimMoney =
            currentPlayer.position <= collectMoneyIndex &&
            newPosition > collectMoneyIndex

          // Move current player to the new position
          currentPlayer.position = newPosition % state.path.squares.length

          // Get information from the square I landed on
          const landedSquare = state.path.squares[currentPlayer.position]

          switch (landedSquare.type) {
            case "BUY":
              state.gamePhase = "TURN_ACTION_BUY"

              // Filter built (hence not buyable) hotels
              const buyableHotels = landedSquare.hotels
                // Remove `null` not caught by TypeScript
                .filter((hotelName) => Boolean(hotelName))
                .map((hotelName) => state.path.hotels[hotelName])
                .filter((hotelDetail) => hotelDetail.progress === "EMPTY")
                .filter(
                  (hotelDetail) => hotelDetail?.owner !== currentPlayer.name
                )
                .map((hotelDetails) => hotelDetails.name)

              currentPlayer.canBuy = buyableHotels

              break
            case "BUILD":
              state.gamePhase = "TURN_ACTION_BUILD"
            default:
              state.gamePhase = "TURN_ACTION_BUILD"
          }

          break
      }

      return state
    },
    claimMoney: (state, action) => {
      const currentPlayer = state.players.find(
        (player) => player.name === action.payload.playerName
      )

      currentPlayer.money += 2000
      currentPlayer.canClaimMoney = false
    },
    buy: (state, action) => {
      const currentPlayer = state.players.find(
        (player) => player.name === action.payload.playerName
      )

      const currentHotel: Hotel = state.path.hotels[action.payload.hotelName]

      currentPlayer.money -= currentHotel.owner
        ? currentHotel.costOfLand / 2
        : currentHotel.costOfLand
      currentPlayer.canBuy = []
      currentHotel.owner = currentPlayer.name
    },
    commitBuild: (state, action) => {
      state.buildingPermission = {
        hotel: action.payload.hotelName,
        progress: action.payload.progress
      }

      state.gamePhase = "TURN_ROLL_PERMISSION_BUILD"
    },
    rollPermissionDice: (state, action) => {
      // Player who rolled
      const currentPlayer = state.players.find(
        (player) => player.name === action.payload.playerName
      )

      // Nothing to do if there's no pending permission
      if (!state.buildingPermission) {
        return
      }

      // Retrieve information from building permission
      const selectedHotel = state.path.hotels[state.buildingPermission.hotel]
      const selectedProgress = state.buildingPermission.progress

      // Compute regular cost
      const currentProgressIndex = Object.keys(
        selectedHotel.buildingPrices
      ).findIndex((key) => key === selectedHotel.progress)

      const accumulatedPrices = {
        ...selectedHotel.buildingPrices
      }

      const keys = Object.keys(accumulatedPrices) as Array<
        keyof typeof accumulatedPrices
      >

      let normalCost = 0
      keys.forEach((type, index) => {
        if (type !== "FACILITIES" && index > currentProgressIndex) {
          normalCost += selectedHotel.buildingPrices[type] || 0
          accumulatedPrices[type] = normalCost
        }
      })

      currentPlayer.permissionRoll = action.payload.roll

      switch (action.payload.roll) {
        case "GREEN":
          selectedHotel.progress = selectedProgress
          currentPlayer.money -= normalCost
          break
        case "FREE":
          selectedHotel.progress = selectedProgress
          break
        case "DOUBLE":
          selectedHotel.progress = selectedProgress
          currentPlayer.money -= normalCost * 2
          break
        case "RED":
          break
      }

      state.buildingPermission = null
    },
    endTurn: (state, action) => {
      // Next player can roll
      const currentPlayer = state.players.find(
        (player) => player.name === action.payload.playerName
      )

      currentPlayer.canBuy = []
      currentPlayer.canClaimMoney = false

      // currentPlayer stays the turn player if he rolled a 6
      if (currentPlayer?.roll !== 6) {
        const currentPlayerIndex = state.players.findIndex(
          (player) => player === currentPlayer
        )

        const nextIndex = (currentPlayerIndex + 1) % state.players.length
        state.turnPlayer = state.players[nextIndex]
      }

      state.gamePhase = "TURN_ROLL_MOVEMENT"
    }
  }
})

export default mainSlice
