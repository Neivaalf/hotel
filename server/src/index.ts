import fs from "fs"
import type { EnhancedStore } from "@reduxjs/toolkit"
import type { Player } from "../../types"

import { configureStore } from "@reduxjs/toolkit"
import express from "express"
import { createServer } from "https"
import { hri } from "human-readable-ids"
import socketIO from "socket.io"

import mainSlice from "./mainSlice"

const app = express()
const server = createServer(
  {
    key: fs.readFileSync("../localhost+1-key.pem"),
    cert: fs.readFileSync("../localhost+1.pem")
  },
  app
)
const io = socketIO(server)

const boards = new Map<string, EnhancedStore>()

// Testing purpose
boards.set(
  "bored-developer-02",
  configureStore({
    reducer: mainSlice.reducer
  })
)

io.on("connection", (socket) => {
  console.log("New connection")
  socket.emit("GET_BOARDS", Array.from(boards.keys()))

  socket.on("ADD_BOARD", () => {
    console.log("ADD_BOARD")
    boards.set(
      hri.random(),
      configureStore({
        reducer: mainSlice.reducer
      })
    )
    io.emit("GET_BOARDS", Array.from(boards.keys()))
  })

  socket.on("CLEAR_BOARDS", () => {
    console.log("CLEAR_BOARDS")
    boards.clear()
    boards.set(
      "bored-developer-02",
      configureStore({
        reducer: mainSlice.reducer
      })
    )

    io.emit("GET_BOARDS", Array.from(boards.keys()))
  })
})

const boardNamespaces = io.of(/^\/[a-zA-Z]+\-[a-zA-Z]+\-[0-9]+$/)

boardNamespaces.on("connection", (socket) => {
  const namespace = socket.nsp
  const boardName = namespace.name.substring(1)

  console.log("New connection to", boardName)

  if (!boards.get(boardName)) return

  const { getState } = boards.get(boardName)

  namespace.emit("GET_BOARD", getState())

  socket.on("NEW_PLAYER", (partialPlayer: Pick<Player, "color" | "name">) => {
    const { dispatch, getState } = boards.get(boardName)

    console.log("NEW_PLAYER", partialPlayer)

    dispatch(mainSlice.actions.addPlayer(partialPlayer))

    namespace.emit("GET_BOARD", getState())
  })

  socket.on("START_GAME", () => {
    const { dispatch, getState } = boards.get(boardName)

    console.log("START_GAME")
    dispatch(mainSlice.actions.startGame(null))

    namespace.emit("GET_BOARD", getState())
    namespace.emit("START_GAME")
  })

  socket.on("ROLL_DICE", (playerName) => {
    const { dispatch, getState } = boards.get(boardName)

    /*/
    const roll = Math.floor(Math.random() * 6) + 1
    /*/
    const roll = 2
    //*/

    console.log("ROLL_DICE", { playerName }, roll)
    dispatch(mainSlice.actions.roll({ playerName, roll }))

    namespace.emit("GET_BOARD", getState())
  })

  socket.on("CLAIM_MONEY", (playerName) => {
    const { dispatch, getState } = boards.get(boardName)

    console.log("CLAIM_MONEY", { playerName })
    dispatch(mainSlice.actions.claimMoney({ playerName }))

    namespace.emit("GET_BOARD", getState())
  })

  socket.on("BUY", (playerName, hotelName) => {
    const { dispatch, getState } = boards.get(boardName)

    console.log("BUY", { playerName, hotelName })
    dispatch(mainSlice.actions.buy({ playerName, hotelName }))

    namespace.emit("GET_BOARD", getState())
  })

  socket.on("COMMIT_BUILD", (playerName, hotelName, progress) => {
    const { dispatch, getState } = boards.get(boardName)

    console.log("COMMIT_BUILD", { playerName, hotelName, progress })
    dispatch(mainSlice.actions.commitBuild({ hotelName, progress }))

    namespace.emit("GET_BOARD", getState())
  })

  socket.on("ROLL_PERMISSION_DICE", (playerName) => {
    const { dispatch, getState } = boards.get(boardName)

    const PERMISSION_DICE = [
      "FREE",
      "DOUBLE",
      "RED",
      "GREEN",
      "GREEN",
      "GREEN"
    ] as const

    /*/
    const roll = PERMISSION_DICE[Math.floor(Math.random() * 6)]
    /*/
    const roll = "DOUBLE"
    //*/

    console.log("ROLL_PERMISSION_DICE", { playerName, roll })
    dispatch(mainSlice.actions.rollPermissionDice({ playerName, roll }))

    namespace.emit("GET_BOARD", getState())
  })
  socket.on("END_TURN", (playerName) => {
    const { dispatch, getState } = boards.get(boardName)

    console.log("END_TURN", { playerName })
    dispatch(mainSlice.actions.endTurn({ playerName }))

    namespace.emit("GET_BOARD", getState())
  })
})

server.listen(3001, () => console.log(`App running at https://localhost:3001`))
