import React from "react"
import socketIO from "socket.io-client"

const useSocket = (boardId?: string) => {
  const [socket, setSocket] = React.useState<SocketIOClient.Socket | null>(null)

  React.useEffect(() => {
    // If it's defined, do not re-create a socket
    if (socket) return

    const { protocol, hostname } = window.location
    const _socket = socketIO(
      `${protocol}//${hostname}:3001${boardId ? `/${boardId}` : ""}`,
      {
        autoConnect: false,
        ca: `-----BEGIN CERTIFICATE-----
        MIICljCCAX4CCQDllUNbdnFG1jANBgkqhkiG9w0BAQUFADANMQswCQYDVQQGEwJm
        cjAeFw0yMDA4MjYyMTIwMDdaFw00ODAxMTEyMTIwMDdaMA0xCzAJBgNVBAYTAmZy
        MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAtBm54LcQpujKQZbh438s
        vO5+yz02kzGzlNYXR/VjNYzMx69eHOa/YzrNnEbLGtMNJO0YsKf2KBRC1ngX9ppE
        X89TiDYiwb3AYsFMlKglaIv1M9iFhwnUWodCwg2VOs+3xnSSbumKBTvspM768nUJ
        Fm3E8UQ6zJGyFdJbHjTQe6fzGNI6bRYpv0QNhv60eSqpCyQZMP75yJgz/txEInrw
        vAxQhRkqzzcztbOGUuDD06CO+v6UtuVzbUD4k6Xmb0NReIhEOWec11+OXT+Qbo9G
        gjdiowtkw5V+Z3x5Q/N3/YH20sMngp2ybHSX7DfjYghQCWw9M47uKC3MypZ5v4v9
        qwIDAQABMA0GCSqGSIb3DQEBBQUAA4IBAQAJshq67mNSGx3nQkJw3KsBrzPxbCvJ
        bQeHm5Ac7NdJ09opNJnQseq9UAPiPQA6DJgSAlbzU+PKCa5N+gT7REOjdyVlrnxP
        D69cH8z7YGvdz4NhDVwdKlPgAV+prR4oYVLOHcYxDoyZ4ZoJ6qQlG5sTpRC4Hc+Q
        v7Tay+bT+o8SbqpW+xdjrOLTX+WZe24WobVzrmOJKea3xTplrlZ2dj6MVRcx/E3q
        PGl1AjUteQkKbrq0886jpSxOo6EJkBLQgXQJ9KnzMc3vxCaZlnW6galicO5AtnY1
        akvw3CjdV9gs/lFJ9d33C5QnImB8NuRYqSgkEuY9e+mYT2SDyq6yMniC
        -----END CERTIFICATE-----`,
        rejectUnauthorized: false
      }
    )

    setSocket(_socket)
    _socket.open()

    console.log(`${protocol}//${hostname}:3001${boardId ? `/${boardId}` : ""}`)
  }, [socket, boardId])

  return socket
}

export default useSocket
