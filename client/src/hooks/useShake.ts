import React from "react"

const useShake = () => {
  const [isShaking, setIsShaking] = React.useState<boolean | null>(null)

  const totalAcceleration = (x: number, y: number, z: number) =>
    Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2) + Math.pow(z, 2))

  React.useEffect(() => {
    let timeoutId: ReturnType<typeof setTimeout> | null

    const handleOnDeviceMotion = (e: DeviceMotionEvent) => {
      const currentTotalAcceleration = totalAcceleration(
        e.acceleration?.x ?? 0,
        e.acceleration?.y ?? 0,
        e.acceleration?.z ?? 0
      )

      if (currentTotalAcceleration > 30 && !timeoutId) {
        setIsShaking(true)

        timeoutId = setTimeout(() => {
          setIsShaking(false)
          if (timeoutId) {
            clearTimeout(timeoutId)
            timeoutId = null
          }
        }, 500)
      }
    }

    if ("ondevicemotion" in window) {
      //   setIsShaking(false)
      window.addEventListener("devicemotion", handleOnDeviceMotion)

      return () => {
        console.log("Clear")
        if (timeoutId) clearTimeout(timeoutId)
        window.removeEventListener("devicemotion", handleOnDeviceMotion)
      }
    } else {
      console.log("event not supported")
    }
  }, [])

  return isShaking
}

export default useShake
