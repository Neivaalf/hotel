import React from "react"
import { useHistory, useParams } from "react-router-dom"
import { Player } from "../../../../../types"

import useSocket from "../../../hooks/useSocket"

const Join = () => {
  const { boardId } = useParams<{ boardId: string }>()
  const history = useHistory()
  const socket = useSocket(boardId)

  const [color, setColor] = React.useState<"BLUE" | "GREEN" | "YELLOW" | "RED">(
    "BLUE"
  )
  const [name, setName] = React.useState<string>("")

  React.useEffect(() => {
    if (!socket) return

    socket.on("START_GAME", () => {
      console.log("START_GAME")
      history.push(`/boards/${boardId}/hands/${name}`)
    })
  }, [history, socket, boardId, name])

  if (!socket) {
    return <div>Connecting</div>
  }

  return (
    <div
      className={`text-center h-screen overflow-auto bg-${color.toLowerCase()}-600 transition-all duration-200 ease-in-out`}
    >
      <h1 className="text-4xl text-white">Join Board: {boardId}</h1>

      <div className={`m-4 p-4 bg-${color.toLowerCase()}-100 rounded`}>
        <div className="flex justify-center">
          <div
            className={`bg-blue-500 hover:bg-blue-400 w-12 h-12 rounded-full flex justify-center items-center transition-all duration-200 ease-in-out
          ${color === "BLUE" ? "border-8 border-blue-600" : ""}`}
            onClick={() => {
              setColor("BLUE")
            }}
          />
          <div
            className={`bg-green-500 hover:bg-green-400 w-12 h-12 rounded-full flex justify-center items-center transition-all duration-200 ease-in-out ml-4
          ${color === "GREEN" ? "border-8 border-green-600" : ""}`}
            onClick={() => {
              setColor("GREEN")
            }}
          />
          <div
            className={`bg-yellow-500 hover:bg-yellow-400 w-12 h-12 rounded-full flex justify-center items-center transition-all duration-200 ease-in-out ml-4
          ${color === "YELLOW" ? "border-8 border-yellow-600" : ""}`}
            onClick={() => {
              setColor("YELLOW")
            }}
          />
          <div
            className={`bg-red-500 hover:bg-red-400 w-12 h-12 rounded-full flex justify-center items-center ml-4 transition-all duration-200 ease-in-out
          ${color === "RED" ? "border-8 border-red-600" : ""}`}
            onClick={() => {
              setColor("RED")
            }}
          />
        </div>

        <div className="flex justify-center mt-4">
          <input
            type="text"
            name="playerName"
            id="playerName"
            placeholder="Xavier Le Magnifique"
            className={`p-2 bg-${color.toLowerCase()}-${
              color === "YELLOW" ? "300" : "200"
            } rounded transition-all duration-200 ease-in-out`}
            onChange={(e) => {
              setName(e.target.value)
            }}
            value={name}
          />

          <button
            className={`bg-${color.toLowerCase()}-500 ml-2 p-2 rounded text-${color.toLowerCase()}-100 transition-all duration-200 ease-in-out`}
            onClick={() => {
              const newPlayer: Pick<Player, "color" | "name"> = {
                color,
                name
              }
              socket.emit("NEW_PLAYER", newPlayer)
            }}
          >
            New Player
          </button>
        </div>
      </div>
    </div>
  )
}

export default Join
