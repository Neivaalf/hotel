import { Board, Hotel } from "../../../../../types"

import React from "react"
import { useParams } from "react-router-dom"

import useSocket from "../../../hooks/useSocket"

import { initialState } from "../../../isomorphic/index"
import Building from "./Building"
import Buying from "./Buying"
import Dice from "./Dice"
import PermissionDice from "./PermissionDice"

const Hand = () => {
  const { boardId, playerName } = useParams<{
    boardId: string
    playerName: string
  }>()
  const socket = useSocket(boardId)
  const [board, setBoard] = React.useState<Board>(initialState)
  const currentPlayer = board.players.find(
    (player) => player.name === playerName
  )

  React.useEffect(() => {
    // Guard if socket is not defined
    if (!socket) return

    socket.on("GET_BOARD", (board: Board) => {
      console.log("GET_BOARD", board)
      setBoard(board)
    })
  }, [socket])

  if (!socket) {
    return <div>Connecting</div>
  }

  const isMyTurn = board.turnPlayer?.name === currentPlayer?.name

  const myHotels = Object.values(board.path.hotels).filter(
    (hotel) => hotel.owner === currentPlayer?.name
  )

  const currentPlayerCanRoll = Boolean(
    currentPlayer?.canRoll ||
      (isMyTurn && board.gamePhase === "TURN_ROLL_MOVEMENT")
  )
  const currentPlayerCanPermissionRoll = Boolean(
    isMyTurn &&
      board.gamePhase === "TURN_ROLL_PERMISSION_BUILD" &&
      board.buildingPermission
  )

  if (!currentPlayer) {
    throw Error(`No player found with name: ${playerName}`)
  }

  return (
    <div
      className={`text-center h-screen overflow-auto bg-${currentPlayer?.color.toLowerCase()}-600`}
    >
      <h1 className="text-4xl text-white">Player: {playerName}</h1>

      <div className="m-4 p-4 bg-white rounded">
        <h2 className="text-2xl">Game phase: {board.gamePhase}</h2>

        <div>isMyTurn: {JSON.stringify(isMyTurn)}</div>

        <div className="flex">
          <div className="flex-1">
            <Dice
              number={currentPlayer.roll}
              color={currentPlayer.color}
              canRoll={currentPlayerCanRoll}
              onRoll={() => {
                socket.emit("ROLL_DICE", playerName)
              }}
            />
          </div>
          <div className="flex-1">
            <PermissionDice
              permissionRoll={currentPlayer.permissionRoll}
              color={currentPlayer.color}
              canRoll={currentPlayerCanPermissionRoll}
              onRoll={() => {
                socket.emit("ROLL_PERMISSION_DICE", playerName)
              }}
            />
          </div>
        </div>

        <div>Money: {currentPlayer?.money}$</div>

        {currentPlayer?.canClaimMoney && (
          <button
            className="bg-purple-600 m-2 p-2 rounded text-white"
            onClick={() => {
              socket.emit("CLAIM_MONEY", playerName)
            }}
          >
            Can Claim Money
          </button>
        )}

        {/* BUYING PHASE */}
        {isMyTurn && board.gamePhase === "TURN_ACTION_BUY" && (
          <Buying
            buyableHotels={currentPlayer?.canBuy || []}
            board={board}
            buyHotel={(hotelName: Hotel["name"]) => {
              socket.emit("BUY", playerName, hotelName)
            }}
          />
        )}

        {/* BUILDING PHASE */}
        {isMyTurn && board.gamePhase === "TURN_ACTION_BUILD" && (
          <Building
            myHotels={myHotels}
            commitBuildHandler={(hotel, progress) => {
              socket.emit("COMMIT_BUILD", playerName, hotel, progress)
            }}
          />
        )}

        {isMyTurn &&
          (board.gamePhase === "TURN_ACTION_BUY" ||
            board.gamePhase === "TURN_ACTION_BUILD" ||
            (board.gamePhase === "TURN_ROLL_PERMISSION_BUILD" &&
              !board.buildingPermission)) && (
            <button
              className="bg-purple-600 m-2 p-2 rounded text-white"
              onClick={() => {
                socket.emit("END_TURN", playerName)
              }}
            >
              End Turn
            </button>
          )}
      </div>

      <div>Players : {JSON.stringify(board.players)}</div>
    </div>
  )
}

export default Hand
