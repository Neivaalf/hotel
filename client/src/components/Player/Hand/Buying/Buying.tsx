import React from "react"
import { Board, Hotel } from "../../../../../../types"

const Buying = ({
  buyableHotels,
  board,
  buyHotel
}: {
  buyableHotels: Array<Hotel["name"]>
  board: Board
  buyHotel: (hotelName: Hotel["name"]) => void
}) => {
  return (
    <>
      {buyableHotels.map((hotelName) => {
        const hotelDetails = board.path.hotels[hotelName]

        return (
          <button
            className="bg-purple-600 m-2 p-2 rounded text-white"
            onClick={() => {
              buyHotel(hotelDetails.name)
            }}
          >
            {hotelDetails.owner ? (
              <>
                Buy {hotelDetails.name}{" "}
                <span className="line-through">
                  ({hotelDetails.costOfLand}$)
                </span>{" "}
                ({hotelDetails.costOfLand / 2}$)
              </>
            ) : (
              <>
                Buy {hotelDetails.name} ({hotelDetails.costOfLand}$)
              </>
            )}
          </button>
        )
      })}
    </>
  )
}

export default Buying
