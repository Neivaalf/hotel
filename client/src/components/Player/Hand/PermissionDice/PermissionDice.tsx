import React from "react"
import { Color, Player } from "../../../../../../types"
import useShake from "../../../../hooks/useShake"

const PermissionDice = ({
  permissionRoll,
  color,
  canRoll,
  onRoll
}: {
  permissionRoll: Player["permissionRoll"]
  color: Color
  canRoll: boolean
  onRoll: () => void
}) => {
  const isShaking = useShake()

  React.useEffect(() => {
    if (canRoll && isShaking) {
      onRoll()
    }
  }, [canRoll, isShaking, onRoll])

  let icon = ""

  switch (permissionRoll) {
    case "GREEN":
      icon = "uil-check-circle"
      break
    case "RED":
      icon = "uil-times-circle"
      break
    case "DOUBLE":
      icon = "uil-usd-circle"
      break
    case "FREE":
      icon = "uil-star"
      break
    default:
      icon = "uil-circle"
  }

  return (
    <div>
      <h1>Permission Dice</h1>
      <i
        className={`uil ${icon}
      text-${color.toLowerCase()}-500
      text-4xl`}
      />

      {canRoll && (
        <button
          className="bg-purple-600 disabled:opacity-50 m-2 p-2 rounded text-white"
          onClick={onRoll}
        >
          Roll Permission Dice
        </button>
      )}
    </div>
  )
}

export default PermissionDice
