import { propTypes } from "qrcode.react"
import React from "react"
import { Hotel } from "../../../../../../types"

const Auction = ({ myHotels }: { myHotels: Hotel[] }) => {
  return (
    <div>
      <h1>Auction - Pick an hotel</h1>

      {myHotels.map((hotel) => (
        <div key={hotel.name}>{hotel.name}</div>
      ))}
    </div>
  )
}

export default Auction
