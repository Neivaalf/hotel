import React from "react"
import { Color } from "../../../../../../types"
import useShake from "../../../../hooks/useShake"

const DICE_NUMBERS = ["one", "two", "three", "four", "five", "six"]

const Dice = ({
  number,
  color,
  canRoll,
  onRoll
}: {
  number: number | null
  color: Color
  canRoll: boolean
  onRoll: () => void
}) => {
  const isShaking = useShake()

  React.useEffect(() => {
    if (canRoll && isShaking) {
      onRoll()
    }
  }, [canRoll, isShaking, onRoll])

  return (
    <div>
      <h1>Dice</h1>
      {number ? (
        <i
          className={`uil uil-dice-${DICE_NUMBERS[number - 1]} 
      text-${color.toLowerCase()}-500
      text-4xl`}
        />
      ) : (
        <i
          className={`uil uil-square-shape 
      text-${color.toLowerCase()}-500
      text-4xl`}
        />
      )}

      {canRoll && (
        <button
          className="bg-purple-600 m-2 p-2 rounded text-white"
          onClick={onRoll}
        >
          Roll
        </button>
      )}
    </div>
  )
}

export default Dice
