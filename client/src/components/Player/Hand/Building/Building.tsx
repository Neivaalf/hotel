import { Hotel } from "../../../../../../types"
import React from "react"

const Building = ({
  myHotels,
  commitBuildHandler
}: {
  myHotels: Hotel[]
  commitBuildHandler: (
    hotel: Hotel["name"],
    progress: Exclude<keyof Hotel["buildingPrices"], "FACILITIES">
  ) => void
}) => {
  const [selectedProgress, setSelectedProgress] = React.useState<{
    hotel: Hotel["name"]
    progress: Exclude<keyof Hotel["buildingPrices"], "FACILITIES">
  } | null>(null)

  const myHotelsWithPrices = myHotels.map((hotel) => {
    const currentProgressIndex = Object.keys(hotel.buildingPrices).findIndex(
      (key) => key === hotel.progress
    )

    const accumulatedPrices = {
      ...hotel.buildingPrices
    }

    const keys = Object.keys(accumulatedPrices) as Array<
      keyof typeof accumulatedPrices
    >

    let currentPrice = 0
    keys.forEach((type, index) => {
      if (type === "FACILITIES") {
        accumulatedPrices[type] = hotel.buildingPrices[type]
      } else if (index <= currentProgressIndex) {
        accumulatedPrices[type] = 0
      } else {
        currentPrice += hotel.buildingPrices[type] || 0
        accumulatedPrices[type] = currentPrice
      }
    })

    return {
      ...hotel,
      accumulatedPrices: accumulatedPrices
    }
  })

  const isProgressSelected = (
    hotelName: Hotel["name"],
    hotelProgress: Exclude<keyof Hotel["buildingPrices"], "FACILITIES">
  ) => {
    return (
      selectedProgress?.hotel === hotelName &&
      selectedProgress.progress === hotelProgress
    )
  }

  return (
    <>
      {myHotelsWithPrices.map((hotel) => (
        <div key={hotel.name} className="mt-4">
          <h1 className="text-center">{hotel.name}</h1>

          <table className="table-auto mx-auto">
            <thead>
              <tr>
                <th className="border border-gray-400 p-2">Progress</th>
                <th className="border border-gray-400 p-2">Price</th>
                <th className="border border-gray-400 p-2">
                  Accumulated Price
                </th>
              </tr>
            </thead>
            <tbody>
              {Object.entries(hotel.accumulatedPrices)
                .filter(([progressLevel]) => progressLevel !== "FACILITIES")
                .map(([progressLevel, progressPrice]) => {
                  // FIXME: Remove type assertion
                  const key = progressLevel as Exclude<
                    keyof Hotel["buildingPrices"],
                    "FACILITIES"
                  >

                  return (
                    <tr
                      key={progressLevel}
                      className={`${
                        progressPrice === 0
                          ? "bg-gray-200 cursor-not-allowed"
                          : "cursor-pointer"
                      }
                      
                      ${
                        isProgressSelected(hotel.name, key) ? "bg-gray-400" : ""
                      }`}
                      onClick={() => {
                        if (progressPrice) {
                          if (!isProgressSelected(hotel.name, key)) {
                            setSelectedProgress({
                              hotel: hotel.name,
                              progress: key
                            })
                          } else {
                            setSelectedProgress(null)
                          }
                        }
                      }}
                    >
                      <td className="border border-gray-400 p-2">
                        {progressLevel}
                      </td>
                      <td className="border border-gray-400 p-2">
                        {hotel.buildingPrices[key]}
                      </td>
                      <td className="border border-gray-400 p-2">
                        {progressPrice}
                      </td>
                    </tr>
                  )
                })}
            </tbody>
          </table>
        </div>
      ))}

      {selectedProgress && (
        <div className="mt-4">
          <button
            className="bg-purple-600 m-2 p-2 rounded text-white"
            onClick={() => {
              commitBuildHandler(
                selectedProgress.hotel,
                selectedProgress.progress
              )
            }}
          >
            Commit
          </button>
        </div>
      )}
    </>
  )
}

export default Building
