import React from "react"
import { Link, useParams } from "react-router-dom"
import QRCode from "qrcode.react"

import { Board } from "../../../../types"
import { initialState } from "../../isomorphic/index"
import useSocket from "../../hooks/useSocket"
import Square from "../Square"

const BoardView = () => {
  const { boardId } = useParams<{
    boardId: string
  }>()
  const socket = useSocket(boardId)
  const [board, setBoard] = React.useState<Board>(initialState)

  const { host, protocol } = window.location

  React.useEffect(() => {
    // Guard if socket is not defined
    if (!socket) return

    socket.on("GET_BOARD", (board: Board) => {
      console.log(board)
      setBoard(board)
    })
  }, [socket])

  if (!socket) {
    return <div>Connecting</div>
  }

  const diceNumbers = ["one", "two", "three", "four", "five", "six"]

  return (
    <div
      className={`text-center h-screen overflow-auto bg-${
        board.turnPlayer?.color.toLowerCase() ?? "purple"
      }-600 transition-all duration-150 ease-in-out`}
    >
      <h1 className="text-4xl text-white">Board: {boardId}</h1>

      {board.gamePhase === "JOINING" && (
        <Link
          to={`/boards/${boardId}/join`}
          target="_blank"
          className="inline-block m-4 p-4 bg-white rounded text-purple-600 font-bold"
        >
          <QRCode
            value={`${protocol}//${host}/boards/${boardId}/join`}
            renderAs="svg"
            className="mb-4"
            size={196}
          />
          Join the board
        </Link>
      )}

      <div className="m-4 p-4 bg-white rounded">
        <h2 className="text-2xl">Players</h2>
        {board && (
          <ul>
            {board.players.map((player) => (
              <li
                key={player.color}
                className={`text-${player.color.toLowerCase()}-500 flex items-center`}
              >
                <i
                  className={`uil ${
                    player.roll
                      ? `uil-dice-${diceNumbers[player.roll - 1]}`
                      : `uil-square-shape`
                  } text-4xl mr-2`}
                />
                {player.name} ({player.money}$)
              </li>
            ))}
          </ul>
        )}

        {board.players.length > 1 && board.gamePhase === "JOINING" && (
          <button
            className="bg-purple-600 m-2 p-2 rounded text-white"
            onClick={() => {
              socket.emit("START_GAME")
            }}
          >
            Start
          </button>
        )}
      </div>

      {board.gamePhase !== "JOINING" && board.gamePhase !== "PLAYER_ORDERING" && (
        <div>
          {board.path.squares.map((square, index) => (
            <React.Fragment key={index}>
              <Square
                board={board}
                square={square}
                players={board.players.filter(
                  (player) => player.position === index
                )}
              />
              {index === board.path.collectMoneyIndex && (
                <div className="m-2 text-white">Collect 2000$</div>
              )}
              {index === board.path.buyEntrancesIndex && (
                <div className="m-2 text-white">Buy entrances</div>
              )}
            </React.Fragment>
          ))}
        </div>
      )}
      <div>Game Phase : {JSON.stringify(board.gamePhase)}</div>

      <details>
        <summary>DEBUG</summary>
        <pre className="text-left">{JSON.stringify(board, null, 2)}</pre>
      </details>
    </div>
  )
}

export default BoardView
