import React from "react"
import { Board, Hotel, Player, Square } from "../../../../types"

const SquareView = ({
  board,
  square,
  players
}: {
  board: Board
  square: Square
  players: Player[]
}) => {
  const getHotelColorClass = (hotel: Hotel["name"]) => {
    const ownerName = board.path.hotels[hotel].owner
    const playerDetails = board.players.find(
      (player) => player.name === ownerName
    )

    return `text-${playerDetails?.color}-500`
  }

  return (
    <div className="flex mx-4 my-1 p-2 rounded bg-white">
      <div
        className={`w-1/3 flex flex-col items-start justify-center text-xs ${
          square.hotels[0]
            ? getHotelColorClass(square.hotels[0])?.toLocaleLowerCase()
            : ""
        }`}
      >
        <div>{square.hotels[0]}</div>
        <div>
          {square.hotels[0] && board.path.hotels[square.hotels[0]].progress}
        </div>
      </div>
      <div className="w-1/3">
        <div className="flex items-center">
          {square.type === "BUILD" && (
            <i className="text-2xl mr-2 uil uil-constructor"></i>
          )}
          {square.type === "FREE_BUILD" && (
            <i className="text-2xl mr-2 uil uil-building"></i>
          )}
          {square.type === "BUY" && (
            <i className="text-2xl mr-2 uil uil-money-stack"></i>
          )}
          {square.type === "FREE_ENTRANCE" && (
            <i className="text-2xl mr-2 uil uil-store"></i>
          )}
          <div className="font-bold">{square.type}</div>
        </div>

        <div>
          {players.map((player) => (
            <div
              key={player.name}
              className={`text-${player.color.toLocaleLowerCase()}-500 text-left`}
            >
              <i className="text-2xl uil uil-car-sideview mr-2"></i>
              {player.name}
            </div>
          ))}
        </div>
      </div>
      <div
        className={`w-1/3 flex flex-col items-end justify-center text-xs ${
          square.hotels[1]
            ? getHotelColorClass(square.hotels[1])?.toLocaleLowerCase()
            : ""
        }`}
      >
        <div>{square.hotels[1]}</div>
        <div>
          {square.hotels[1] && board.path.hotels[square.hotels[1]].progress}
        </div>
      </div>
    </div>
  )
}

export default SquareView
