import React from "react"
import { Link } from "react-router-dom"
import useSocket from "../../hooks/useSocket"

const Boards = () => {
  const socket = useSocket()
  const [boardIds, setBoardIds] = React.useState<string[]>([])

  React.useEffect(() => {
    // Guard if socket is not defined
    if (!socket) return

    socket.on("GET_BOARDS", (boardIds: string[]) => {
      setBoardIds(boardIds)
    })
  }, [socket])

  if (!socket) {
    return <div>Connecting</div>
  }

  return (
    <div className="text-center h-screen overflow-auto bg-purple-600">
      <h1 className="text-4xl text-white">Hotel</h1>

      <div className="m-4 p-4 bg-white rounded">
        <button
          className="bg-purple-600 m-2 p-2 rounded text-white"
          onClick={() => {
            socket.emit("ADD_BOARD")
          }}
        >
          Add Board
        </button>

        <button
          className="text-purple-600 m-2 p-2"
          onClick={() => {
            socket.emit("CLEAR_BOARDS")
          }}
        >
          Clear Boards
        </button>

        <div>
          List of Boards:
          {boardIds && (
            <ul>
              {boardIds.map((boardId) => (
                <li key={boardId}>
                  <Link
                    to={`/boards/${boardId}`}
                    target="_blank"
                    className="text-purple-600 font-bold"
                  >
                    {boardId}
                  </Link>
                </li>
              ))}
            </ul>
          )}
        </div>
      </div>
    </div>
  )
}

export default Boards
