import React from "react"
import {
  Redirect,
  BrowserRouter as Router,
  Route,
  Switch
} from "react-router-dom"

import Board from "./components/Board"
import Boards from "./components/Boards"
import Join from "./components/Player/Join"
import Hand from "./components/Player/Hand"

const App = () => (
  <Router>
    <Switch>
      <Route path="/boards/:boardId/hands/:playerName">
        <Hand />
      </Route>
      <Route path="/boards/:boardId/join">
        <Join />
      </Route>
      <Route path="/boards/:boardId">
        <Board />
      </Route>
      <Route path="/boards">
        <Boards />
      </Route>
      <Route path="*">
        <Redirect to="/boards" />
      </Route>
    </Switch>
  </Router>
)

export default App
