import { Hotel } from "../../../../types"

export const FUJIYAMA: Hotel = {
  progress: "EMPTY",
  name: "FUJIYAMA",
  costOfLand: 1000,
  entrancePrice: 100,
  buildingPrices: {
    MAIN_BUILDING: 2200,
    EXTENSION1: 1400,
    EXTENSION2: 1400,
    FACILITIES: 500
  },
  rentDuePerNight: {
    MAIN_BUILDING: 50,
    EXTENSION1: 50,
    EXTENSION2: 100,
    FACILITIES: 250
  }
}

export const BOOMERANG: Hotel = {
  progress: "EMPTY",
  name: "BOOMERANG",
  costOfLand: 500,
  entrancePrice: 100,
  buildingPrices: {
    MAIN_BUILDING: 1800,
    FACILITIES: 250
  },
  rentDuePerNight: {
    MAIN_BUILDING: 400,
    FACILITIES: 600
  }
}

export const ETOILE: Hotel = {
  progress: "EMPTY",
  name: "ETOILE",
  costOfLand: 3000,
  entrancePrice: 250,
  buildingPrices: {
    MAIN_BUILDING: 3300,
    EXTENSION1: 2200,
    EXTENSION2: 1800,
    EXTENSION3: 1800,
    EXTENSION4: 1800,
    FACILITIES: 4000
  },
  rentDuePerNight: {
    MAIN_BUILDING: 150,
    EXTENSION1: 300,
    EXTENSION2: 300,
    EXTENSION3: 300,
    EXTENSION4: 450,
    FACILITIES: 750
  }
}

export const PRESIDENT: Hotel = {
  progress: "EMPTY",
  name: "PRESIDENT",
  costOfLand: 3500,
  entrancePrice: 250,
  buildingPrices: {
    MAIN_BUILDING: 5000,
    EXTENSION1: 3000,
    EXTENSION2: 2250,
    EXTENSION3: 1750,
    FACILITIES: 5000
  },
  rentDuePerNight: {
    MAIN_BUILDING: 200,
    EXTENSION1: 400,
    EXTENSION2: 600,
    EXTENSION3: 800,
    FACILITIES: 1200
  }
}

export const ROYAL: Hotel = {
  progress: "EMPTY",
  name: "ROYAL",
  costOfLand: 2500,
  entrancePrice: 200,
  buildingPrices: {
    MAIN_BUILDING: 3600,
    EXTENSION1: 2600,
    EXTENSION2: 1800,
    EXTENSION3: 1800,
    FACILITIES: 3000
  },
  rentDuePerNight: {
    MAIN_BUILDING: 150,
    EXTENSION1: 300,
    EXTENSION2: 300,
    EXTENSION3: 450,
    FACILITIES: 600
  }
}

export const WAIKIKI: Hotel = {
  progress: "EMPTY",
  name: "WAIKIKI",
  costOfLand: 2500,
  entrancePrice: 200,
  buildingPrices: {
    MAIN_BUILDING: 3500,
    EXTENSION1: 2500,
    EXTENSION2: 2500,
    EXTENSION3: 1750,
    EXTENSION4: 1750,
    FACILITIES: 2500
  },
  rentDuePerNight: {
    MAIN_BUILDING: 200,
    EXTENSION1: 350,
    EXTENSION2: 500,
    EXTENSION3: 500,
    EXTENSION4: 650,
    FACILITIES: 1000
  }
}

export const TAJ_MAHAL: Hotel = {
  progress: "EMPTY",
  name: "TAJ_MAHAL",
  costOfLand: 1500,
  entrancePrice: 100,
  buildingPrices: {
    MAIN_BUILDING: 2400,
    EXTENSION1: 1000,
    EXTENSION2: 500,
    FACILITIES: 1000
  },
  rentDuePerNight: {
    MAIN_BUILDING: 100,
    EXTENSION1: 100,
    EXTENSION2: 200,
    FACILITIES: 300
  }
}

export const SAFARI: Hotel = {
  progress: "EMPTY",
  name: "SAFARI",
  costOfLand: 2000,
  entrancePrice: 150,
  buildingPrices: {
    MAIN_BUILDING: 2600,
    EXTENSION1: 1200,
    EXTENSION2: 1200,
    FACILITIES: 2000
  },
  rentDuePerNight: {
    MAIN_BUILDING: 100,
    EXTENSION1: 100,
    EXTENSION2: 250,
    FACILITIES: 500
  }
}
