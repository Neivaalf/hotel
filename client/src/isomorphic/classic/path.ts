import { Path } from "../../../../types"

import {
  FUJIYAMA,
  BOOMERANG,
  ETOILE,
  PRESIDENT,
  ROYAL,
  WAIKIKI,
  TAJ_MAHAL,
  SAFARI
} from "./titleDeeds"

const OLD_PATH: Path = {
  initialMoney: 12000,
  collectMoneyIndex: 6,
  buyEntrancesIndex: 25,
  hotels: {
    FUJIYAMA,
    BOOMERANG,
    ETOILE,
    PRESIDENT,
    ROYAL,
    WAIKIKI,
    TAJ_MAHAL,
    SAFARI
  },
  squares: [
    {
      type: "FREE_BUILD",
      hotels: [null, null]
    },
    {
      type: "BUILD",
      hotels: [null, "FUJIYAMA"]
    },
    {
      type: "BUY",
      hotels: ["BOOMERANG", "FUJIYAMA"]
    },
    {
      type: "BUILD",
      hotels: ["BOOMERANG", "FUJIYAMA"]
    },
    {
      type: "BUY",
      hotels: ["BOOMERANG", "FUJIYAMA"]
    },
    {
      type: "BUILD",
      hotels: ["BOOMERANG", "FUJIYAMA"]
    },
    {
      type: "FREE_ENTRANCE",
      hotels: [null, "FUJIYAMA"]
    },
    {
      type: "BUILD",
      hotels: [null, null]
    },
    {
      type: "BUY",
      hotels: [null, "ETOILE"]
    },
    {
      type: "BUY",
      hotels: ["PRESIDENT", "ETOILE"]
    },
    {
      type: "FREE_BUILD",
      hotels: ["PRESIDENT", "ETOILE"]
    },
    {
      type: "BUY",
      hotels: ["PRESIDENT", "ROYAL"]
    },
    {
      type: "BUILD",
      hotels: ["PRESIDENT", "ROYAL"]
    },
    {
      type: "BUY",
      hotels: ["PRESIDENT", "ROYAL"]
    },
    {
      type: "BUILD",
      hotels: ["PRESIDENT", "ROYAL"]
    },
    {
      type: "BUY",
      hotels: ["PRESIDENT", "ROYAL"]
    },
    {
      type: "BUILD",
      hotels: ["WAIKIKI", "ROYAL"]
    },
    {
      type: "BUY",
      hotels: ["WAIKIKI", "ROYAL"]
    },
    {
      type: "FREE_ENTRANCE",
      hotels: ["WAIKIKI", "ROYAL"]
    },
    {
      type: "BUILD",
      hotels: ["WAIKIKI", "ROYAL"]
    },
    {
      type: "BUY",
      hotels: ["WAIKIKI", "ROYAL"]
    },
    {
      type: "BUY",
      hotels: ["TAJ_MAHAL", "ETOILE"]
    },
    {
      type: "BUILD",
      hotels: ["TAJ_MAHAL", "ETOILE"]
    },
    {
      type: "BUY",
      hotels: ["TAJ_MAHAL", "ETOILE"]
    },
    {
      type: "FREE_BUILD",
      hotels: ["TAJ_MAHAL", "ETOILE"]
    },
    {
      type: "BUILD",
      hotels: ["TAJ_MAHAL", null]
    },
    {
      type: "BUILD",
      hotels: ["SAFARI", null]
    },
    {
      type: "BUILD",
      hotels: ["SAFARI", null]
    },
    {
      type: "BUY",
      hotels: ["SAFARI", "ETOILE"]
    },
    {
      type: "FREE_ENTRANCE",
      hotels: ["SAFARI", "ETOILE"]
    },
    {
      type: "BUILD",
      hotels: ["SAFARI", null]
    }
  ]
}

export default OLD_PATH
