import { Board, Player } from "../../../types"

import classicPath from "./classic/path"

const DEBUG = true

// DEBUG
const players: Player[] = [
  {
    color: "BLUE",
    name: "LeXav",
    permissionRoll: null,
    canRoll: false,
    canClaimMoney: false,
    position: -1,
    money: 12000,
    canBuy: [],
    roll: 3
  },
  {
    color: "GREEN",
    name: "LeFlav",
    permissionRoll: null,
    canRoll: false,
    canClaimMoney: false,
    position: 3,
    money: 5000,
    canBuy: [],
    roll: 5
  }
]

// /DEBUG

export const initialState: Board = {
  gamePhase: DEBUG ? "TURN_ROLL_PERMISSION_BUILD" : "JOINING",
  players: DEBUG ? players : [],
  turnPlayer: DEBUG ? players[1] : null,
  buildingPermission: DEBUG
    ? {
        hotel: "WAIKIKI",
        progress: "EXTENSION4"
      }
    : null,
  path: DEBUG
    ? {
        ...classicPath,
        hotels: {
          ...classicPath.hotels,
          BOOMERANG: {
            ...classicPath.hotels.BOOMERANG,
            progress: "EMPTY",
            owner: "LeFlav"
          },
          WAIKIKI: {
            ...classicPath.hotels.WAIKIKI,
            progress: "EXTENSION2",
            owner: "LeFlav"
          },
          ETOILE: {
            ...classicPath.hotels.ETOILE,
            progress: "EMPTY",
            owner: "LeXav"
          }
        }
      }
    : classicPath
}
